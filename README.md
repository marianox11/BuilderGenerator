# BuilderGenerator

## Allows to generate a Builder class from a .NET Assembly.

Usage:

 -f, --FileName    Required. Binary file name containing the Assembly/type for the generator.

  -t, --TypeName    Required. Name of Type for the generator.

  -o, --Output      Output file (.cs) name and location.

  --help            Display this help screen.

  --version         Display version information.

  Sample:

  _BuilderGenerator.exe -f "MyLibrary.dll" -t sampleClass -o sampleClassBuilder.cs_


  Assuming Mylibrary.dll contains the following class:

```csharp  
namespace sampleNameSpace
{
    public class sampleClass
    {
        public int intProp { get; set; }
        public string stringProp { get; set; }
        int a;
        string stringMethod() => "hello world!";
        public anotherClass anotherClassProp { get; set; }
    }
}
```

The output would be:

```csharp  
namespace sampleNameSpace
{
	public class sampleClassBuilder 
	{
		private sampleClass _sampleclass;
		public sampleClassBuilder()
		{
			_sampleclass = new sampleClass();
		}
		public sampleClassBuilder intProp(System.Int32 intProp)
		{
			_sampleclass.intProp = intProp;
			return this;
		}
		public sampleClassBuilder stringProp(System.String stringProp)
		{
			_sampleclass.stringProp = stringProp;
			return this;
		}
		public sampleClassBuilder anotherClassProp(sampleNameSpace.anotherClass anotherClassProp)
		{
			_sampleclass.anotherClassProp = anotherClassProp;
			return this;
		}
		public sampleClass Build() => _sampleclass;
	}
}
```

And stored in the file: sampleClassBuilder.cs 

## Contributor
Mariano Rius 
