﻿namespace sampleNameSpace
{
    public class sampleClass
    {
        public int intProp { get; set; }
        public string stringProp { get; set; }
        int intPrivProp;
        string stringMethod() => "hello world!";
        public anotherClass anotherClassProp { get; set; }
    }

    public class anotherClass
    {
        public string Name { get; set; }
    }
}
