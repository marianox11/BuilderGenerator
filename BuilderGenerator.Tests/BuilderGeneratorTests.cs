using System;
using System.IO;
using Xunit;

namespace BuilderGenerator.Tests
{
    public class BuilderGeneratorTests
    {
        [Fact]
        public void ParsesAndIdentifiesAssemblyItems()
        {
            string inputType = "sampleClass";
            string outputFile = "testOutput.cs";
            string assemblyFileName = @$"../../../../sampleClass/bin/Debug/net8.0/{inputType}.dll";
            BuilderGenApplication application = new BuilderGenApplicationBuilder()
                .AssemblyFileName(assemblyFileName)
                .TypeName("sampleClass")
                .OutputFile(outputFile)
                .Build();
            var props = application.Parse();
            Assert.True(props.Length == 3);
            Assert.True(props[1].Name.CompareTo("stringProp") == 0);
            Assert.True(props[1].PropertyType.FullName.CompareTo("System.String") == 0);
            if (File.Exists(outputFile))
                File.Delete(outputFile);
            application.GenerateBuilder();
            Assert.True(File.Exists(outputFile));
	    var txt = File.ReadAllText(outputFile);
	    Console.WriteLine("the output file is:");
	    Console.WriteLine(txt);
            File.Delete(outputFile);
        }
    }
}
