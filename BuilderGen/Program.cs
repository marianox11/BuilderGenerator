﻿using CommandLine;

namespace BuilderGenerator
{
    class Program
    {
        class Options
        {
            [Option('f', "FileName", Required = true, HelpText = "Binary file name containing the Assembly/type for the generator.")]
            public string FileName { get; set; }

            [Option('t', "TypeName", Required = true, HelpText = "Name of Type for the generator.")]
            public string TypeName { get; set; }

            [Option('o', "Output", Required = false, HelpText = "Output file (.cs) name and location.")]
            public string OutputFileName { get; set; }
        }

        static int Main(string[] args)
        {
            string assemblyFileName = "";
            string typeName = "";
            string outputFileName="";

            var parserResult = Parser.Default.ParseArguments<Options>(args)
                .WithParsed(options =>
                {
                    assemblyFileName = options.FileName;
                    typeName = options.TypeName;
                    outputFileName = options.OutputFileName;
                });
            if (parserResult is NotParsed<Options>)
                return -1;

            BuilderGenApplication application = new BuilderGenApplicationBuilder()
                .AssemblyFileName(assemblyFileName)
                .TypeName(typeName)
                .OutputFile(outputFileName)
                .Build();

            var props = application.Parse();
            application.GenerateBuilder();

            return 0;
        }
    }
}
